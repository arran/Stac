using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Stac.Authentication;
using Stac.Models;

namespace Stac.Controllers.Authentication
{
    [Authorize]
    [Route("authentication")]
    public class AuthenticationController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly JSONWebTokenGenerator _jsonWebTokenGenerator;
        private readonly ILogger<AuthenticationController> _logger;

        public AuthenticationController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            JSONWebTokenGenerator jsonWebTokenGenerator,
            ILogger<AuthenticationController> logger)
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._jsonWebTokenGenerator = jsonWebTokenGenerator;
            this._logger = logger;
        }

        /// <summary>
        /// Sign in to the application with username and password pair,
        /// receiving an Application Cookie when successful.
        /// </summary>
        [AllowAnonymous]
        [HttpPost("signin")]
        public async Task<IActionResult> SignIn(string username, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(username, password, true, false);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        /// <summary>
        /// Sign out the currently logged on user.
        /// </summary>
        [HttpPost("signout")]
        public async Task<IActionResult> SignOut(string returnUrl = "/")
        {
            var user = await _userManager.GetUserAsync(User);
            await _signInManager.SignOutAsync();

            _logger.LogInformation($"Signed out [ {user.UserName} ]");
            return Redirect(returnUrl);
        }

        /// <summary>
        /// Return a JWT token for the currently logged in User.
        /// </summary>
        [HttpGet("jwt")]
        public async Task<JSONWebToken> GetJWT()
        {
            var user = await _userManager.GetUserAsync(User);
            return new JSONWebToken
            {
                Token = _jsonWebTokenGenerator.GenerateJSONWebToken(user)
            };
        }
    }
}