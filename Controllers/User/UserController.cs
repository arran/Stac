using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Stac.Models;

namespace Stac.Controllers.Users
{
    [Authorize]
    [Route("user")]
    public class UserController : Controller
    {
        public UserManager<IdentityUser> _userManager { get; }
        public ILogger<UserController> _logger { get; }

        public UserController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ILogger<UserController> logger)
        {
            this._userManager = userManager;
            this._logger = logger;
        }

        /// <summary>
        /// Return the currently logged in User's Identity.
        /// </summary>
        [HttpGet]
        public async Task<UserInformation> CurrentUserInfo()
        {
            return await _userManager.GetUserAsync(User);
        }

        /// <summary>
        /// Register a new User with a username and password pair.
        /// </summary>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(string username, string password)
        {
            var newUser = new IdentityUser(username);
            var result = await _userManager.CreateAsync(newUser, password);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }
    }
}
