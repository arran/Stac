using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using Microsoft.AspNetCore.Authorization;
using System;

namespace Stac.Controllers
{
    [Authorize]
    [Route ("image")]
    public class ImageController : Controller
    {

        private readonly IHttpClientFactory _clientFactory;
        private readonly ILogger<ImageController> _logger;

        public ImageController(
            ILogger<ImageController> logger,
            IHttpClientFactory clientFactory)
        {
            _logger = logger;
            _clientFactory = clientFactory;
        }

        /// <summary>
        /// Return an upside-down picture of a Cat.
        /// </summary>
        [HttpGet("cat")]
        public async Task<IActionResult> Cat(string tag = "")
        {
            var client = _clientFactory.CreateClient("cat");

            // Build the requestUrl
            string requestUrl = Path.Join("cat", tag);

            HttpResponseMessage response;
            using (client)
            {
                try
                {
                    response = await client.GetAsync(requestUrl);
                }
                catch (HttpRequestException ex)
                {
                    _logger.LogError(ex, "Failed attempt to retreive Cat image.");
                    return StatusCode(502);
                }
            }

            // If the response was a success, flip and return the image.
            if (response.IsSuccessStatusCode)
            {
                byte[] data = await response.Content.ReadAsByteArrayAsync();
                byte[] flippedImage = flipImage(data);
                return File(flippedImage, "image/png");
            }
            else
            {
                // As this is essentially a proxy
                // It's appropriate to forward the server's StatusCode on failure.
                return StatusCode((int)response.StatusCode);
            }

        }

        /// <summary>
        /// Flip an image along the vertical axis
        /// </summary>
        private byte[] flipImage(byte[] photoBytes)
        {
            using (MemoryStream stream = new MemoryStream())
            using (Image image = Image.Load(photoBytes))
            {
                image.Mutate(operation =>
                    operation.Flip(FlipMode.Vertical)
                );

                image.SaveAsPng(stream);
                return stream.ToArray();
            }
        }
    }
}