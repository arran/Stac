# Stac (Cats) 🐈
AspNetCore RESTful API to return upside-down cat pictures.


## Features
- Authenticated access to **upside-down** cat pictures with tag support! eg. (`angry`, `sad`, `beans`)
- Interactive Swagger Documentation
- Simultaneous **Cookie** and **JWT** Authentication


## Running
| .NET Core CLI  | Docker |
| -------------- | ------ |
|`dotnet restore`|`docker build . -t stac`
|`dotnet run`    |`docker run -p 8080:80 --rm stac`


## Environment Variables / Configuration
Configuration can be retreived from either `appsettings.json` or Environment Variables.
- `JWT_SIGNING_KEY`: Signing key to generate and verify JSON web tokens. `string`
- `JWT_ISSUER`: Issuer of the token `string`
- `JWT_AUDIENCE`: Intended recipient of the token. `string`
- `JWT_VALIDITY`: Validity of the token, in minutes. `int`


## Quick Start
I recommend using the interactive Swagger documentation located at the root node: `/`.

If the CLI is your style, try: [HTTPpie](https://httpie.org/) & [HTTPie-JWT](https://github.com/teracyhq/httpie-jwt-auth).

**Register a new User**

`POST /user username=<username> password=<password>`

**Sign In**

`POST /authentication/signin username=<username> password=<password>`

**Enjoy upside-down cat pictures**

`GET /image/cat`

`GET /image/cat?tag=beans`

**Try JWT**

`GET /authentication/jwt`

`GET /image/cat Bearer=<token>`


## Development Notes
### Project Structure
```shell
├── Controllers
│   ├── Authentication
│   │   └── AuthenticationController.cs
│   ├── User
│   │   └── UserController.cs
│   └── ImageController.cs
├── Models
│   └── UserInformation.cs
├── Program.cs
└── Startup.cs
```

For me, defining a directory structure is an important decision when beginning a new project. An intuitive structure aids the development process by making components painless to find and modify.

In this project the directory names are based on the RESTful API design, for example `/authentication => Controllers/Authentication` - This design helps determine what class is responsible for particular HTTP routes.


### Project Philosophy
I have attempted to write as little code as possible, placing an importance on layout and readability which makes the project easily accessible to future developers.

Where possible, I have researched and integrated relevant packages. Utilizing these pre-existing solutions saves time, reduces code complexity and delegates maintenance to the respective package authors.

One concrete example is the image manipulation library - Instead of creating a function to flip the cat image, I opted for [ImageSharp](https://github.com/SixLabors/ImageSharp).

### Git
Well defined guidelines regarding commits and their messages add a new informational dimension to a project, one that helps paint a picture of how and why code has changed.

I emphasize:

- Including concise, well defined changes.
- Using an imperative mood in commit messages.
- Capitalizing the beginning of the subject line.


## Code Snippets

### HTTPie

Open a cat image using a JWT.
```shell
$ T=<token>
$ http --auth-type=jwt --auth="${T}" localhost:8080/image/cat --output cat && xdg-open cat
```

Get `UserInformation` using a JWT.
```shell
$ T=<token>
$ http --auth-type=jwt --auth="${T}" localhost:8080/user
```
