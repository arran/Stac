using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Stac.Authentication
{
    public class JSONWebTokenGenerator
    {

        private SymmetricSecurityKey _securityKey;

        private string _issuer;

        private string _audience;

        private TimeSpan _tokenValidity;

        private ILogger<JSONWebTokenGenerator> _logger;


        public JSONWebTokenGenerator(
            SymmetricSecurityKey securityKey,
            string issuer,
            string audience,
            TimeSpan tokenValidity,
            ILogger<JSONWebTokenGenerator> logger
        )
        {
            _issuer = issuer;
            _audience = audience;
            _securityKey = securityKey;
            _tokenValidity = tokenValidity;
            _logger = logger;

        }

        /// <summary>
        /// Generate a JSONWebToken for a specified User.
        /// </summary>
        /// <returns></returns>
        public string GenerateJSONWebToken(IdentityUser user)
        {
            _logger.LogInformation($"Creating Token for User [ {user.UserName} ]");
            var credentials = new SigningCredentials(_securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                    GetUserIdClaim(user)
                };

            var token = new JwtSecurityToken(
                _issuer,
                _audience,
                claims,
                expires: DateTime.Now + _tokenValidity,
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Return the UserIdClaim
        /// With this Claim present, _userMananger.GetUserAsync() functions.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private Claim GetUserIdClaim(IdentityUser user)
        {
                IdentityOptions options = new IdentityOptions();
                return new Claim(options.ClaimsIdentity.UserIdClaimType, user.Id.ToString());
        }
    }
}