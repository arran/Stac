using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Stac.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Stac
{
    public class Startup
    {
        /// <summary>
        /// Current configuration.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Extend and apply a supplied configuration.
        /// </summary>
        /// <param name="configuration">Configuration</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddEnvironmentVariables()
                .Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            #region Identity (User Management)
            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityDbContext>();

            services.AddDbContext<IdentityDbContext>(options =>
            {
                options.UseInMemoryDatabase("MyTemporaryDatabase");
            });

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;
            });

            #endregion

            #region Application Cookie (Authentication)
            services.ConfigureApplicationCookie(options => {
                // Intercept and override default login event behavior to avoid redirects to pages that
                // do not exist.
                options.Events.OnRedirectToLogin = async (context) => {
                    context.Response.StatusCode = 401;
                    await Task.CompletedTask;
                };

                options.Events.OnRedirectToAccessDenied = async (context) => {
                    context.Response.StatusCode = 403;
                    await Task.CompletedTask;
                };
            });
            #endregion

            #region JWT Authentication

            var jwtSigningKey = GetRequiredConfigurationVariable<string>("JWT_SIGNING_KEY");
            var jwtIssuer = GetRequiredConfigurationVariable<string>("JWT_ISSUER");
            var jwtAudience = GetRequiredConfigurationVariable<string>("JWT_AUDIENCE");
            var jwtValidity = GetRequiredConfigurationVariable<int>("JWT_VALIDITY");

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSigningKey));

            services.AddSingleton<JSONWebTokenGenerator>(sp =>
                {
                    return new JSONWebTokenGenerator(
                        securityKey,
                        jwtIssuer,
                        jwtAudience,
                        TimeSpan.FromMinutes(jwtValidity),
                        sp.GetRequiredService<ILogger<JSONWebTokenGenerator>>());
                }
            );

            services.AddAuthentication()
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = jwtIssuer,
                        ValidAudience = jwtAudience,
                        IssuerSigningKey = securityKey
                    };
                });

            #endregion

            #region Authorization
            // Configure both JWT and Cookie Authentication to accepted when Authorizing requests.
            services.AddAuthorization(options =>
            {
                    var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(
                        IdentityConstants.ApplicationScheme,
                        JwtBearerDefaults.AuthenticationScheme);

                    defaultAuthorizationPolicyBuilder = defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();
                    options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
            });
            #endregion

            #region HTTP Clients
            services.AddHttpClient("cat", config =>
            {
                config.BaseAddress = new Uri("https://cataas.com/");
            });
            #endregion

            #region Swagger Generation (Documentation)
            services.AddSwaggerGen(options =>
            {
                OpenApiInfo interface_info = new OpenApiInfo { Title = "Stac", Version = "v1", };
                options.SwaggerDoc("v1", interface_info);

                // Generate documentation based on the Docstrings provided above each Controller endpoint.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });
            #endregion

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            #region Swagger UI (Documentation)
            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                // Viewing the root `/` will return the Swagger interface.
                options.RoutePrefix = string.Empty;
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Stac");
            });
            #endregion

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Attempt to retreive a Configuration Variable
        /// Throwing an ArgumentNullException if not present.
        /// </summary>
        private T GetRequiredConfigurationVariable<T>(string key)
        {
            return Configuration.GetValue<T>(key)
                ?? throw new ArgumentNullException($"The environment variable {key} has not been set.");
        }

    }
}
