using System;
using Microsoft.AspNetCore.Identity;

namespace Stac.Models
{   
    [Serializable]
    public class JSONWebToken
    {
        public string Token { get; set; }
    }
}