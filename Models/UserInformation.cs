using System;
using Microsoft.AspNetCore.Identity;

namespace Stac.Models
{   
    [Serializable]
    public class UserInformation
    {
        public string Username { get; set; }

        public string Id { get; set; }

        /// <summary>
        /// Serialize an IdentityUser into a subset of informational fields.
        /// </summary>
        /// <param name="user"></param>
        public static implicit operator UserInformation(IdentityUser user)
        {
            return new UserInformation
            {
                Username = user.UserName,
                Id = user.Id,
            };
        }
    }
}